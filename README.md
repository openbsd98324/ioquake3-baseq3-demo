# ioquake3-baseq3-demo


## Installation 

Files ready to use for ioquake on raspios, raspbian, debian with /dev/fb0 kernel, and on retropie : 


A compiled binary for retropie-4.5.1-rpi2_rpi3.img.gz is available at: 

https://gitlab.com/openbsd98324/ioquake3-baseq3-demo/-/raw/main/linux/aarch64/retropie-4.5.1-rpi2_rpi3/opt/ioquake3.arm-retropie-retropie-4.5.1-rpi2_rpi3-default-v2.tar.gz

The Demo Pack files: 
http://files.retropie.org.uk/archives/Q3DemoPaks.zip


Make sure that you have the files: 

```
opt/retropie/ports/quake3/
opt/retropie/ports/quake3/baseq3
opt/retropie/ports/quake3/ioq3ded.arm
opt/retropie/ports/quake3/ioquake3.arm
home/pi/.q3a/baseq3/
home/pi/.q3a/baseq3/pak4.pk3
home/pi/.q3a/baseq3/pak2.pk3
home/pi/.q3a/baseq3/games.log
home/pi/.q3a/baseq3/pak1.pk3
home/pi/.q3a/baseq3/pak6.pk3
home/pi/.q3a/baseq3/pak0.pk3
home/pi/.q3a/baseq3/pak8.pk3
home/pi/.q3a/baseq3/q3config.cfg
home/pi/.q3a/baseq3/pak7.pk3
home/pi/.q3a/baseq3/pak5.pk3
home/pi/.q3a/baseq3/pak3.pk3
home/pi/.q3a/baseq3/server.cfg
```


## Run Quake 3 on Raspberry PI

Alt+Ctrl+F1, login to pi, just type into the console (no X11)

/opt/retropie/ports/quake3/ioquake3.arm





## Releases 

### Stable 

https://gitlab.com/openbsd98324/ioquake3-baseq3-demo/-/raw/main/linux/aarch64/retropie-4.5.1-rpi2_rpi3/opt/ioquake3.arm-retropie-retropie-4.5.1-rpi2_rpi3-default-v2.tar.gz

(no X11 necessary, it will use /dev/fb0)

Working well on : 
Linux retropie 4.14.98-v7+ #1200 SMP Tue Feb 12 20:27:48 GMT 2019 armv7l GNU/Linux


 


